lazy val jvm = project.settings(
		name := "Workshop Reactive Programming (NS & Craftsmen)",
		version := "1.0",
		scalaVersion := "2.12.1",
		libraryDependencies += "io.reactivex" % "rxjava" % "1.2.4"
)

scalaVersion := "2.12.1"

// Loads the project named java at sbt startup
onLoad in Global := (Command.process("project jvm", _: State)) compose (onLoad in Global).value