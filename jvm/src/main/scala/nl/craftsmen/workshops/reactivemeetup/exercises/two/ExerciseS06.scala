package nl.craftsmen.workshops.reactivemeetup.exercises.two

import java.util.Optional

import nl.craftsmen.workshops.reactivemeetup.domain.railway.MotionType.MotionType
import nl.craftsmen.workshops.reactivemeetup.domain.railway.{MotionType, RailwayStation, TrainMetrics, TrainMovementAction}
import nl.craftsmen.workshops.reactivemeetup.util.RailwayStreams
import nl.craftsmen.workshops.reactivemeetup.util.Utils._
import rx.Observable
import rx.functions.Func2

object ExerciseS06 extends App {

  type JDouble = java.lang.Double
  // This is a bonus exercise that is bit more involved. Given are two streams. The first is trainMetrics$ stream, which is the same
  // stream you have already used in exercise 2.5. The second stream is the velocity$ stream that is derived from the trainMetrics$
  // stream (which is actually the solution of exercise 2.5). This exercise is split into three smaller exercises, which all need to
  // be solved to accomplish the final goal: showing from which railway station the train departs and at which station it arrives.
  val trainMetrics$: Observable[TrainMetrics] = RailwayStreams.trainMetrics$
  // ASSIGNMENT: Using the velocity$ stream, define a new motion$ stream that describes the motion of the train using one of the
  // the following constants:
  //  - MotionType.ACCELERATING    The train is accelerating (its velocity is increasing).
  //  - MotionType.DECELERATING    The train is decelerating (its velocity is decreasing).
  //  - MotionType.CONSTANT_SPEED  The train is moving at constant speed.
  //  - MotionType.STATIONARY      The train is not moving at all.
  //
  // HINT: First try to get a stream that emits the acceleration for two subsequent emits of the velocity$ stream.
  //
  // HINT: Use an epsilon value of 0.1 to determine if the train is accelerating or decelerating, i.e. the train is not accelerating /
  // decelerating if the value of the acceleration is greater than -0.1 and less than 0.1.
  //
  // HINT: You cannot use the stream of acceleration values alone to differentiate between a train that is stationary and a train
  // moving at constant speed, since the acceleration is 0 in both cases.
  //
  // HINT: Uncomment the "logAndWaitFor(motion$)" line below to test your stream.
  val velocity$: Observable[java.lang.Double] = RailwayStreams.velocity$(trainMetrics$)
  val motion$: Observable[MotionType] = velocity$.buffer(2, 1).filter((velocities) => velocities.size() > 1).
    map[JDouble](velocities => velocities.get(1) - velocities.get(0))
    .zipWith[JDouble, MotionType](velocity$.skip(1),
    ((acceleration, velocity) => {
      if (Math.abs(acceleration) < 0.1) if (velocity < 0.1) MotionType.STATIONARY else MotionType.CONSTANT_SPEED
      else if (acceleration < 0) MotionType.DECELERATING else MotionType.ACCELERATING
    }): Func2[JDouble, JDouble, MotionType]).distinctUntilChanged

  // logAndWaitFor(motion$)
  // ASSIGNMENT: With the motion$ stream, defined above, it is possible to detect whether a train is departing or arriving. Use the
  // following constants and conditions to define a trainAction$ stream that indicates when a train is departing or arriving at a
  // railway station:
  //  - TrainMovementAction.DEPARTING  If the motion changes from STATIONARY to ACCELERATING.
  //  - TrainMovementAction.ARRIVING   If the motion changes from DECELERATING to STATIONARY.
  // For other motion transitions that do not match one the condition above the trainAction$ stream should not emit any value.
  //
  // HINT: We'll assume trains do not encounter any malfunctions or obstructions on the railway network and as such we can safely
  // assume that whenever a train starts decelerating it will always stop at a railway station.
  //
  // HINT: Use Optional.empty() or null in case irrelevant motion transitions are detected and filter them out in a second step.
  //
  // HINT: Uncomment the "logAndWaitFor(trainAction$)" line below to test your stream.

  val trainAction$: Observable[TrainMovementAction] =
    motion$.buffer(2, 1).filter(motions => motions.size > 1).map[Optional[TrainMovementAction]](motions =>
      (motions.get(0), motions.get(1)) match {
        case (MotionType.STATIONARY, MotionType.ACCELERATING) => Optional.of(TrainMovementAction.DEPARTING)
        case (MotionType.DECELERATING, MotionType.STATIONARY) => Optional.of(TrainMovementAction.ARRIVING)
        case _ => Optional.empty()
      }
    ).filter(_.isPresent()).map[TrainMovementAction](_.get())

  // logAndWaitFor(trainAction$)
  // ASSIGNMENT: Using the trainAction$ and trainMetrics$ streams, define a new message$ stream that emits a message whenever a
  // train arrives or departs at a railway station including the name of that station. The output should be something like this:
  //  - Departing from stationA
  //  - Arriving at stationB
  //
  // HINT: For this exercise you have to use an operator that you (probably) have not used before in the other exercises.
  //
  // HINT: Use RailwayStation.closestTo(position) to find the railway station closest to the specified LatLong coordinate.
  //
  // HINT: Uncomment the "logAndWaitFor(messages$)" line below to test your stream.

  val messages$: Observable[String] = trainAction$.withLatestFrom(trainMetrics$, (action, trainMetrics: TrainMetrics) =>
    (if (action == TrainMovementAction.DEPARTING) "Departing from " else "Arriving at ") +
      RailwayStation.closestTo(trainMetrics.position))

  logAndWaitFor(messages$)

  private def logAndWaitFor(stream$: Observable[_]) {
    stream$.subscribe(println(_))
    waitForStreamToComplete(stream$)
  }
}
