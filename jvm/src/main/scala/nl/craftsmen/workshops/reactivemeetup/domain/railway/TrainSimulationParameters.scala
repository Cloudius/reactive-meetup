package nl.craftsmen.workshops.reactivemeetup.domain.railway

case class TrainSimulationParameters
(trainId: String, maxVelocity: Double, acceleration: Double, timeDilation: Double, tickFrequency: Int)