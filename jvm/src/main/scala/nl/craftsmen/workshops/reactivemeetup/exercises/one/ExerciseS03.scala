package nl.craftsmen.workshops.reactivemeetup.exercises.one

import nl.craftsmen.workshops.reactivemeetup.util.ExampleStreams
import nl.craftsmen.workshops.reactivemeetup.util.Utils._

object ExerciseS03 extends App {
	val number$ = ExampleStreams.number$
	// ASSIGNMENT: Create a new stream based on the number$ stream that only emits even numbers.
	val evenNumber$ = number$.filter(i => i % 2 == 0)
	// If implemented correctly, the application will output the following numbers: 4, 6, 2, 2, 4, 8
	evenNumber$.subscribe(println(_))
	waitForStreamToComplete(evenNumber$)
}
