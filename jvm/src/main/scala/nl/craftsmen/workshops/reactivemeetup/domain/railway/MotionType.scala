package nl.craftsmen.workshops.reactivemeetup.domain.railway

object MotionType extends Enumeration {
  type MotionType = Value
  val STATIONARY, ACCELERATING, DECELERATING, CONSTANT_SPEED = Value
}