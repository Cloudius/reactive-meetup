package nl.craftsmen.workshops.reactivemeetup.domain.railway

import java.util.concurrent.TimeUnit
import rx.Observable

class StationaryTrainSimulation(val position: LatLong, val stationaryTime: Double) extends TrainSimulation {
	def trainMetrics$(parameters: TrainSimulationParameters, startTime: Long): Observable[TrainMetrics] = {
		val tickDelay = 1000.0 / parameters.tickFrequency
		val requiredNumberOfFrames = Math.ceil(stationaryTime * 1000 / (tickDelay * parameters.timeDilation)).toInt
		Observable.interval(tickDelay.round, TimeUnit.MILLISECONDS).take(requiredNumberOfFrames).map[TrainMetrics](frameIndex => {
			val elapsedTime = ((frameIndex + 1) * tickDelay) / 1000.0 * parameters.timeDilation
			TrainMetrics(parameters.trainId, startTime + (elapsedTime * 1000).toLong, position)
		})
	}
}
