package nl.craftsmen.workshops.reactivemeetup.exercises.one

import nl.craftsmen.workshops.reactivemeetup.util.ExampleStreams
import nl.craftsmen.workshops.reactivemeetup.util.Utils._
import rx.Observer

object ExerciseS02 extends App {

	val number$ = ExampleStreams.numbersWithError$
	// ASSIGNMENT: Subscribe to the number$ stream and log each event (next, error, complete) to the console.
	//number$.???
	// If you have completed the assignment successfully you should see the an output that is similar to:
	//
	// next: 1
	// next: 9
	// next: 4
	// next: 7
	// error: java.lang.RuntimeException: uh oh! an error!
	//
	// This also illustrates that if a stream emits an error event, it will no longer emit a complete event, so a stream
	// either terminates with an error event or a complete event.
	number$.subscribe(new Observer[Integer]() {
		def onCompleted() {
			System.out.println("Done")
		}

		def onError(e: Throwable) {
			System.out.println(e.toString)
		}

		def onNext(n: Integer) {
			System.out.println(n)
		}
	})
	waitForStreamToComplete(number$)

}
