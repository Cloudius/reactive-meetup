package nl.craftsmen.workshops.reactivemeetup.exercises.two

import nl.craftsmen.workshops.reactivemeetup.util.Utils._
import rx.Observable
import rx.subjects._

object ExerciseS02 extends App {
	// ASSIGNMENT: Create a Subject and make sure that the program prints the following output:
	//  - after 1 second: RxJava is cool :)
	//  - after 2 seconds: So reactive!
	//  - after 3 seconds: Much stream!
	//  - after 4 seconds: Goodbye!
	// You are only allowed to modify the lines with the ??? comment. Do not change the order the statements. Also make sure that the
	// program terminates and does not wait indefinitely for the stream to complete.
	//
	// HINT: Think of which kind of Subject you need to produce the desired output.
	val subject = ReplaySubject.create[String]
	val yells = Seq("RxJava is cool ☺", "So reactive!", "Much stream!", "Goodbye!")

	for (yell <- yells) runDelayedAsync( (yells.indexOf(yell) + 1) * 999, () => subject.onNext(yell))

	runDelayedAsync(4010, () => subject.onCompleted())
	logAndWaitFor(subject)

	private def logAndWaitFor(stream$: Observable[_]) {
		stream$.subscribe(println(_))
		waitForStreamToComplete(stream$)
	}
}
