package nl.craftsmen.workshops.reactivemeetup.exercises.one

import nl.craftsmen.workshops.reactivemeetup.util.ExampleStreams
import nl.craftsmen.workshops.reactivemeetup.util.Utils._

object ExerciseS04 extends App {
	val number$ = ExampleStreams.number$
	// ASSIGNMENT: Use to number$ to define a new stream that only contains those numbers that are prime.
	//
	// HINT: You can make use of the utility function Utils.isPrime to check if a given number is a prime number.
	val primeNumber$ = number$.filter(i => isPrime(i))
	// If implemented correctly, the application will output the following numbers: 1, 7, 2, 2, 7, 3
	primeNumber$.subscribe(println(_))
	waitForStreamToComplete(primeNumber$)

}

