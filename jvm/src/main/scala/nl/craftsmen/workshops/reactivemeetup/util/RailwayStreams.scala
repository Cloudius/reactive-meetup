package nl.craftsmen.workshops.reactivemeetup.util

import java.text.{ParseException, SimpleDateFormat}
import java.util
import java.util.Collections
import java.util.concurrent.TimeUnit

import nl.craftsmen.workshops.reactivemeetup.domain.railway._
import nl.craftsmen.workshops.reactivemeetup.util.Utils.sample
import rx.Observable

/**
  * Created by FransAdm on ''16-12-24.
  **/
object RailwayStreams {
  private val DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS dd-MM-YYYY")
  //  private var gateCheckEvent$ = null

  def gateCheckEvent$: Observable[GateCheckEvent] = {
    def singleGateCheckEvent$(isCheckin: Boolean, delay: Long) =
      Observable.from(Collections.singletonList(
        new GateCheckEvent(isCheckin, System.currentTimeMillis + delay, RailwayStation.AMR))).delay(delay, TimeUnit.MILLISECONDS)

    Observable.merge(util.Arrays.asList(
      singleGateCheckEvent$(true, 233),
      singleGateCheckEvent$(true, 978),
      singleGateCheckEvent$(false, 1313),
      singleGateCheckEvent$(true, 2105),
      singleGateCheckEvent$(false, 3643),
      singleGateCheckEvent$(false, 4411),
      singleGateCheckEvent$(true, 5556),
      singleGateCheckEvent$(false, 8123),
      singleGateCheckEvent$(false, 9722),
      singleGateCheckEvent$(true, 10880)))
  }

  def personalCheckinsCheckouts$: Observable[GateCheckEvent] = sample(Observable.from(util.Arrays.asList(
    new GateCheckEvent(true, parseDate("08:04:11.345 16-12-2016"), RailwayStation.UTR),
    new GateCheckEvent(false, parseDate("08:41:03.409 16-12-2016"), RailwayStation.AMS),
    new GateCheckEvent(true, parseDate("17:44:56.122 16-12-2016"), RailwayStation.AMS),
    new GateCheckEvent(false, parseDate("18:49:04.123 16-12-2016"), RailwayStation.DH),
    new GateCheckEvent(true, parseDate("22:15:44.616 16-12-2016"), RailwayStation.DH),
    new GateCheckEvent(true, parseDate("08:03:54.883 17-12-2016"), RailwayStation.UTR),
    new GateCheckEvent(false, parseDate("08:39:21.512 17-12-2016"), RailwayStation.AMS))), 500)

  private def parseDate(dateString: String) = try
    DATE_FORMAT.parse(dateString).getTime

  catch {
    case e: ParseException => {
      throw new RuntimeException(e)
    }
  }

  def travelCostMatrix: TravelCostMatrix = TravelCostMatrix.builder.define(RailwayStation.UTR, RailwayStation.AMS, 7.50).define(RailwayStation.UTR, RailwayStation.DH, 11.00).define(RailwayStation.DH, RailwayStation.AMS, 11.50).build

  def trainMetrics$: Observable[TrainMetrics] = {
    val simulationParameters = TrainSimulationParameters("1042", 140 / 3.6, 2 / 3.6, 40.0, 100)
    val simulation = new CompositeTrainSimulation(
      new StationaryTrainSimulation(RailwayStation.AMR.getLocation, 60.0),
      new TrainJourneySimulation(RailwayStation.AMR.getLocation, RailwayStation.UTR.getLocation),
      new StationaryTrainSimulation(RailwayStation.UTR.getLocation, 60.0))
    simulation.trainMetrics$(simulationParameters, System.currentTimeMillis)
  }

  def velocity$(trainMetrics$: Observable[TrainMetrics]): Observable[java.lang.Double] =
    trainMetrics$.buffer(10, 5).filter(measurements => measurements.size() > 1).map[Double](measurements => {
      val first = measurements.get(0)
      val last = measurements.get(measurements.size - 1)
      val elapsedTime = last.timestamp - first.timestamp
      val distance = last.position.distanceTo(first.position)
      distance * 1000 / elapsedTime
    }).map(velocity => velocity * 3.6)
}
