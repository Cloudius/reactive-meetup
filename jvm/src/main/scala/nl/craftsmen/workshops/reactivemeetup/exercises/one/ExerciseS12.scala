package nl.craftsmen.workshops.reactivemeetup.exercises.one

import nl.craftsmen.workshops.reactivemeetup.util.ExampleStreams
import nl.craftsmen.workshops.reactivemeetup.util.Utils._
import rx.functions.Func2

object ExerciseS12 extends App {

	// The point$ stream represents a the number of earned points by a player/team for a number of games.
	val point$ = ExampleStreams.point$
	// ASSIGNMENT: Use the point$ stream to display the number of points for each game, including the total number of
	// points earned so far. The output should be formatted as following:
	//
	//   Points: 5 - total: 11
	//
	// HINT: Use the scan operator, followed by the zipWith operator.


	val subtotals$ = point$.scan(_ + _).
		zipWith[Integer, String](point$, ((total, points) => s"Points: $points - total: $total"): Func2[Integer, Integer, String])

	// If implemented correctly, the application should display the following output:
	//
	// Points: 0 - total: 0
	// Points: 3 - total: 3
	// Points: 0 - total: 3
	// Points: 3 - total: 6
	// Points: 1 - total: 7
	// Points: 3 - total: 10
	// Points: 0 - total: 10
	// Points: 0 - total: 10
	// Points: 3 - total: 13
	// Points: 0 - total: 13
	subtotals$.subscribe(println(_))
	waitForStreamToComplete(subtotals$)

}
