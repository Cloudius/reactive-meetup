package nl.craftsmen.workshops.reactivemeetup.domain.railway

import java.text.SimpleDateFormat
import java.util.Date

object GateCheckEvent {
  private val DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS dd-MM-YYYY")
}

case class GateCheckEvent(isCheckIn: Boolean, private val _timestamp: Long, railwayStation: RailwayStation) {
  final private val timestamp = new Date(_timestamp)

  def this(isCheckIn: Boolean, railwayStation: RailwayStation) {
    this(isCheckIn, System.currentTimeMillis, railwayStation)
  }

  def isCheckOut: Boolean = !isCheckIn

  def getTimestamp: Date = timestamp

  def getRailwayStation: RailwayStation = railwayStation

  override def toString: String =
    s"${GateCheckEvent.DATE_FORMAT.format(timestamp)} - ${if (isCheckIn) "check-in" else "check-out"} @ $railwayStation"
}
