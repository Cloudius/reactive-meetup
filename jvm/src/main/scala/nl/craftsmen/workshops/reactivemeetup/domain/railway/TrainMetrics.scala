package nl.craftsmen.workshops.reactivemeetup.domain.railway

import java.text.SimpleDateFormat
import java.util.Date

case class TrainMetrics(trainId: String, timestamp: Long, position: LatLong) {
  override def toString: String =
    s"( trainId = $trainId, time = ${TrainMetrics.TIMESTAMP_DATE_FORMAT.format(new Date(timestamp))}, position = $position)"
}

object TrainMetrics {
  private val TIMESTAMP_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS dd-MM-YYYY")
}

