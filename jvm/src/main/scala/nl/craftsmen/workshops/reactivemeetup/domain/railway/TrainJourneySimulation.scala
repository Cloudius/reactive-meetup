package nl.craftsmen.workshops.reactivemeetup.domain.railway

import java.util.concurrent.TimeUnit
import rx.Observable

class TrainJourneySimulation(val startPosition: LatLong, val destinationPosition: LatLong) extends TrainSimulation {
  def trainMetrics$(parameters: TrainSimulationParameters, startTime: Long): Observable[TrainMetrics] = {
    val totalDistance: Double = startPosition.distanceTo(destinationPosition)
    val accelerationTime: Double = parameters.maxVelocity / parameters.acceleration
    val accelerationDistance: Double = 0.5 * parameters.acceleration * accelerationTime * accelerationTime
    // TODO adjust acceleration time and distance when unable to achieve max velocity for short distances.
    val unacceleratedDistance: Double = totalDistance - 2 * accelerationDistance
    val unacceleratedTime: Double = unacceleratedDistance / parameters.maxVelocity
    val totalTime: Double = 2 * accelerationTime + unacceleratedTime
    val tickDelay: Double = 1000.0 / parameters.tickFrequency
    val requiredNumberOfFrames: Int = Math.ceil(totalTime * 1000 / (tickDelay * parameters.timeDilation)).toInt
    Observable.interval(tickDelay.round, TimeUnit.MILLISECONDS).take(requiredNumberOfFrames).map((frameIndex) => {
      val elapsedTime = ((frameIndex + 1) * tickDelay) / 1000.0 * parameters.timeDilation
      var distance = .0
      if (elapsedTime < accelerationTime) distance = 0.5 * parameters.acceleration * elapsedTime * elapsedTime
      else if (elapsedTime < unacceleratedTime + accelerationTime) distance = accelerationDistance + parameters.maxVelocity * (elapsedTime - accelerationTime)
      else {
        val t = Math.min(accelerationTime, elapsedTime - accelerationTime - unacceleratedTime)
        distance = Math.min(totalDistance, accelerationDistance + unacceleratedDistance + parameters.maxVelocity * t - 0.5 * parameters.acceleration * t * t)
      }
      val normalizedDistance = distance / totalDistance
      val currentPosition = startPosition.interpolate(destinationPosition, normalizedDistance)
      new TrainMetrics(parameters.trainId, startTime + (elapsedTime * 1000).toLong, currentPosition)
    })
  }
}
