package nl.craftsmen.workshops.reactivemeetup.exercises.one

import nl.craftsmen.workshops.reactivemeetup.util.ExampleStreams
import nl.craftsmen.workshops.reactivemeetup.util.Utils._

object ExerciseS01 extends App {

	// The number$ stream below will emit short a sequence of numbers with an interval of 1 second.
	val number$ = ExampleStreams.number$
	// ASSIGNMENT: Subscribe to the number$ stream and print each number to the console.
	//
	// HINT: The following two expressions are equivalent:
	//   (value) -> System.out.print(value)
	//   System.out::println
	number$.subscribe(println(_))
	// The standard RxJava scheduler uses daemon threads by default. To make sure the application does not terminate
	// immediately we have to block the main thread until the stream completes. For this reason we created a the
	// waitForStreamToComplete utility method.
	waitForStreamToComplete(number$)

}
