package nl.craftsmen.workshops.reactivemeetup.exercises.one

import nl.craftsmen.workshops.reactivemeetup.util.ExampleStreams
import nl.craftsmen.workshops.reactivemeetup.util.Utils._

object ExerciseS11 extends App {

	val shape$ = ExampleStreams.shape$
	// ASSIGNMENT: Use the reduce operator to find the shape that has the largest surface area. Store the result in the
	// largestShape$ stream.
	//
	// HINT: Reduce does not emit intermediate results, it may therefore take some time before the result is available and
	// printed to the console.
	val largestShape$ = shape$.reduce((a, b) => if (a.calculateArea() > b.calculateArea()) a else b)
	// If implemented correctly, only one line is printed to console:
	//   Circle 40: 5026.548245743669

	largestShape$.map[String](i => s"${i.getName} : ${i.calculateArea()}").subscribe(println(_))

	waitForStreamToComplete(largestShape$)
	waitForStreamToComplete(largestShape$)

}
